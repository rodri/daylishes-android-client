package com.daylishes.client.fragments;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.daylishes.client.activities.MainActivity;
import com.daylishes.client.fragments.adapters.DayListAdapter;

public class DayListFragment extends ListFragment {

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		setListAdapter(new DayListAdapter(getActivity()));
	}

	@Override
	public void onListItemClick(ListView listview, View view, int position,
			long id) {
		super.onListItemClick(listview, view, position, id);

		DayDetailFragment dayDetailFrag = new DayDetailFragment();

		((MainActivity) getActivity()).navigate(dayDetailFrag);
	}
}
