package com.daylishes.client.activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.daylishes.client.R;

public class MenuListAdapter extends BaseAdapter {

	private Context context;
	private String[] menuOpts = { "Pratos do dia", "Perfil", "Comentários"

	};

	public MenuListAdapter(Context context) {

		this.context = context;
	}

	@Override
	public int getCount() {
		return menuOpts.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = convertView;

		if (view == null) {
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			view = vi.inflate(R.layout.menu_list_row, null);
		}
		
		TextView tv = (TextView) view.findViewById(R.id.menu_title);
		tv.setText(menuOpts[position]);

		return view;
	}

}
