package com.daylishes.client.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.daylishes.client.R;
import com.daylishes.client.activities.adapters.MenuListAdapter;
import com.daylishes.client.fragments.DayListFragment;

public class MainActivity extends FragmentActivity {

	private DrawerLayout drawer;
	private ListView menuListView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.drawer_layout);

		// Add list frament
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.add(R.id.fragmentContainer, new DayListFragment());
		ft.commit();

		drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

		menuListView = (ListView) findViewById(R.id.left_drawer);

		// Set the adapter for the list view
		menuListView.setAdapter(new MenuListAdapter(this));
		menuListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long id) {
				
				drawer.closeDrawers();
			}
		});

		View leftMenuButton = findViewById(R.id.leftMenuButton);
		leftMenuButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				drawer.openDrawer(menuListView);
			}
		});
	}

	public void navigate(Fragment destination) {

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
				R.anim.slide_in_left, R.anim.slide_out_right);
		ft.replace(R.id.fragmentContainer, destination);
		ft.addToBackStack(null);

		ft.commit();
	}
}
